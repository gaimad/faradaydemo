let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.js('resource/js/faraday.js', 'public/')
.webpackConfig({
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'resource/js/'),
        }
    }
})
.sass('resource/scss/style.scss', 'public/')
.copy("public/", "../../public/vendor/faraday/");
