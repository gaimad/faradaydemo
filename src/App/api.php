<?php

Route::prefix('faraday-api')->group(function () {
    Route::group(['middleware' => ['api']], function () {
      Route::group(['namespace' => 'Faraday\Platform\Controllers'], function() {
        Route::get('list/{resource}', 'IndexController@list')->name('list');
        Route::get('new/{resource}', 'IndexController@new')->name('new');
        Route::put('{resource}/create', 'IndexController@create')->name('new');
        Route::post('{resource}/show', 'IndexController@show')->name('show');
        Route::put('{resource}/update', 'IndexController@update')->name('update');
        Route::post('{resource}/delete', 'IndexController@delete')->name('delete');
      });
    });
});
