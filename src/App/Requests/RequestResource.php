<?php

namespace Faraday\App\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Faraday\App\Traits\TraitValidation;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class RequestResource extends FormRequest
{
    use TraitValidation;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->validationRules()['rules'];
    }

    public function messages()
    {
        return $this->validationRules()['messages'];
    }

    public function failedValidation(Validator $validator)
    {
      throw new HttpResponseException(response()->json(['errors' => $validator->errors()]));
    }
}
