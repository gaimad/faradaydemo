<?php

Route::group(['namespace' => 'Faraday\App\Controllers'], function() {
	Route::get('/faraday/{path?}', 'IndexController@index')->where('path', '.*');
});
