<?php

namespace Faraday\Fields;

abstract class Component {

  public static function init (...$arguments) {
    return new static(...$arguments);
  }

}
