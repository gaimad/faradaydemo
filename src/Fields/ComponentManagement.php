<?php
namespace Faraday\Fields;

abstract class ComponentManagement extends Component {

  public $hideInTable = false;
  public $rules = [];
  public $defaultValue = false;

  public function hideInTable ($status = true) {
    $this->hideInTable = $status;
    return $this;
  }

  public function defaultValue ($defaultValue) {
    $this->defaultValue = $defaultValue;
    return $this;
  }

  public static function inputRules (...$arguments) {
    $this->rules = $arguments;
    return $this;
  }

}
