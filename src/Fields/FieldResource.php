<?php
namespace Faraday\Fields;

abstract class FieldResource extends ComponentManagement {

  public $name = '';
  public $column = '';
  public $type = '';
  public $hideInTable = false;

  public function __construct ($label, $column = null, $type = 'text') {
      $this->name = $label;
      $this->column = $column == null || $column == '' ? $label : $column;
      $this->type = $type;
  }

}
