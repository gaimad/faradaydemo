import instance from './instance'

const Api = {
  resource (model) {
    return instance
      .get(`list/${model}`)
  },
  newResource (model) {
    return instance
      .get(`new/${model}`)
  },
  createResource (data) {
    return instance
      .put(`${data.model}/create`, data.values)
  },
  getSingleData (data) {
    return instance
      .post(`${data.model}/show`, {id: data.id})
  },
  updateResource (data) {
    return instance
      .put(`${data.model}/update`, data.values)
  },
  deleteResource (data) {
    return instance
      .post(`${data.model}/delete`, data)
  }
}

export default Api
