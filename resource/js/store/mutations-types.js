let icon = ''
let group = ''
function getMutationLabel(icon, group, name) {
  return icon + ' ' + group + ' : ' + name
}

export const RESOURCE_REQUEST = getMutationLabel(icon, group, 'Data')
export const RESOURCE_NEW = getMutationLabel(icon, group, 'New')
export const RESOURCE_CREATE = getMutationLabel(icon, group, 'Create')
export const RESOURCE_UPDATE = getMutationLabel(icon, group, 'Update')
export const RESOURCE_SHOW = getMutationLabel(icon, group, 'Single Data')
export const RESOURCE_DELETE = getMutationLabel(icon, group, 'Delete')
