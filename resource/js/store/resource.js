import {
  RESOURCE_REQUEST,
  RESOURCE_NEW,
  RESOURCE_CREATE,
  RESOURCE_SHOW,
  RESOURCE_UPDATE,
  RESOURCE_DELETE
} from './mutations-types'
import Api from '@/api'

const state = {
  info: false,
  tableData: false,
  tableHeader: false,
  components: false,
  errors: false
}
const getters = {

}
const actions = {
  [RESOURCE_REQUEST]: ({commit, dispatch}, model) => {
    return Api.resource(model)
      .then(resp => {
        commit('setInfo', resp.data.info)
        commit('setTable', resp.data)
        return resp.data
      })
  },
  [RESOURCE_NEW]: ({commit, dispatch}, model) => {
    return Api.newResource(model)
      .then(resp => {
        commit('setComponents', resp.data.fields)
        return resp.data
      })
  },
  [RESOURCE_CREATE]: ({commit, dispatch}, data) => {
    return Api.createResource(data)
      .then(resp => {
        if(resp.data.errors) {
          commit('setErrors', resp.data.errors)
        }
        return resp.data
      })
  },
  [RESOURCE_SHOW]: ({commit, dispatch}, data) => {
    return Api.getSingleData(data)
      .then(resp => {
        commit('setComponents', resp.data.fields)
        return resp.data
      })
  },
  [RESOURCE_UPDATE]: ({commit, dispatch}, data) => {
    return Api.updateResource(data)
      .then(resp => {
        if(resp.data.errors) {
          commit('setErrors', resp.data.errors)
        }
        return resp.data
      })
  },
  [RESOURCE_DELETE]: ({commit, dispatch}, data) => {
    return Api.deleteResource(data)
      .then(resp => {
        return resp.data
      })
  },
}

const mutations = {
  setInfo (state, resp) {
    state.info = resp
  },
  setTable (state, resp) {
    state.tableData = resp.resources
    state.tableHeader = resp.rows
  },
  setComponents (state, resp) {
    state.components = resp
  },
  setErrors (state, resp) {
    state.errors = resp
  },
  resourceRemove (state, value) {
    state.tableData = state.tableData.filter(item => {
      return item.id != value
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
