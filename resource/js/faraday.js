window.Vue = require('vue');
import router from '@/router'
import store from '@/store'

const requireComponent = require.context(
  './components',
  true,
  /_[\w-]+\.vue$/

)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)
  let tag = componentConfig.default.tag
  Vue.component(
    tag,
    componentConfig.default || componentConfig
  )
})

Vue.mixin({
  methods: {
    openModal (id) {
      $('#exampleModal').modal("show")
      this.$store.commit('global/setId', id)
    },
    closeModal () {
      $('#exampleModal').modal("hide")
      this.$store.commit('global/setId', '')
    }
  }
})

new Vue({
    el: '#faraday',
    router,
    store
});
